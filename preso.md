# Varying Vagrant Vagrants

## OC WordPress, April 2014 Developers Meetup

### David Arceneaux

http://davidthemachine.org

---

# So ya wanna work on a WordPress site?

## How to do?

Wow, such options, much choice, so confuse

----

## Roll your own

Install the right versions of PHP with modules, database server, & webserver

Can be daunting for the beginner, and not always matching what the site will be
hosted on.

Problems of Mac/PC dev environment vs. Linux host

----

## Pre-packaged configs of the same

MAMP, WAMP, XAMPP, or DesktopServer

* Takes the confusion out of how to get a dev environment up,

* Again, mismatch between development and production environments

----

# Virtualization!

* A self-contained full server environment, running on your local machine,

* Can exactly match what's on the production server

* Possible to do with Free-Libre software, and powerful enough for billable work

* * *

Lots of Big Scary Words, Such Afraid, Wow

---

# the Land o' Vagrant

## The ingredients

Pick the appropriate versions from the webpages, or through your preferred package manager:

* **VirtualBox**, virtualization engine that Vagrant will use, https://www.virtualbox.org/wiki/Downloads

* **Vagrant**, from http://vagrantup.com/downloads

  * The vagrant-hostsupdater plugin:<br> `vagrant plugin install vagrant-hostsupdater`

* (Optional) **Git**, from http://git-scm.com

---

# The main course

* Varying Vagrant Vagrants, download the latest from
  https://github.com/varying-vagrant-vagrants/vvv/releases or <br>
  `git checkout https://github.com/Varying-Vagrant-Vagrants/VVV.git`

* Vagrant boxes are configured through a Vagrantfile, which has a recipe for
  building a box that's configured for what you need to run

----

## Helpers, using Auto Site Setup

* **vagrant-vbguest** &mdash; helps keep the box up to date<br>
  `vagrant plugin install vagrant-vbguest`

* **Site Bootstrap Demo** &mdash; example of how to pre-package WordPress
  site install kit<br>
  https://github.com/simonwheatley/vvv-demo-1

* **Site Boostrap Builder** &mdash; A way to automate building the site
  <br>
  https://github.com/aliso/vvv-site-wizard

---

# Demonstration

## Q&A
